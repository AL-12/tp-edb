#!/bin/bash
#Nom auteur : S.L
#Date création : 27/04/2021
#Date dernière modif :
#Description du script : Change le mot de passe d'un utilisateur donné.
#Invocation du script : ./ChangePass.sh

read -p "De quel utilisateur souhaitez-vous modifier le mot de passe ? " login
passwd $login && chage -d 0 $login

