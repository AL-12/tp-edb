#!/bin/bash
#Nom auteur : S.L
#Date création : 27/04/2021
#Date dernière modif :
#Description du script : Ajout un utilisateur ou un groupe selon réponse de l'utilisateur.
#			Le GID et/ou UID est demandé.
#Invocation du script : ./AddUser.sh (depuis case switch)

read -p "souhaitez-vous créer un groupe ou un utilisateur ? " rep

if [ $rep = "groupe" ]
then
	echo "vous souhaitez créer un groupe."
	echo "Entrez le GID. (Entre 0 et 499 si groupe système.
	Supérieur à 500 si groupe standard.)"
	read gid
	read -p "Entrez le nom du groupe à créer " gname
	groupadd -g $gid $gname
elif [ $rep = "utilisateur" ]

then
	echo "Vous souhaitez créer un utilisateur."
	echo "Entrez le GID. (Entre 0 et 499 si groupe système.
	Supérieur à 500 si groupe standard.)"
        read gid
        read -p "Entrez le nom de l'utilisateur à créer " username
        echo "Entre l'UID. (Entre 0 et 499 si utilisateur système.
	Supérieur à 500 si utilisateur standard.)"
	read uid
	useradd -g $gid -u $uid $username

else echo "Réponse incorrecte"
fi
